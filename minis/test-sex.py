#!/usr/bin/env python
from __future__ import division
from sys import *
from glob import glob
from subprocess import Popen, PIPE

def make_list(filename):
    with open(filename) as list_file:
        return {
            line.split()[0]: line.split()[4]
            for line in list_file
        }

if __name__ == "__main__":
    people = make_list(argv[1])
    folder = argv[2]
    for person in sorted(people.keys()):
        filename = glob("{folder}/{person}*.gz".format(**globals()))[0]
        sex = people[person]
        uniqs = Popen(
            "zcat {filename} | awk '{{print $1}}' | uniq -c".format(**globals()),
            shell = True, stdout = PIPE
        )
        X, X_passed = 0, False
        Y, Y_passed = 0, False
        for line in iter(uniqs.stdout.readline, ""):
            fields = line.split()
            if fields[1] == "X":
                X = int(fields[0])
                X_passed = True
            elif fields[1] == "Y":
                Y = int(fields[0])
                Y_passed = True
            if X_passed and Y_passed:
                break
        if X == 0:
            percentage = "NA"
        else:
            percentage = Y/X
        print("{person}\t{sex}\t{percentage}".format(**globals()))
