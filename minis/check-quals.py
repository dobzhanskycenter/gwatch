#!/usr/bin/env python
from sys import *

def yielder(bim, vcf):
    with open(bim) as bim_handle, open(vcf) as vcf_handle:
        vcf_chrom, vcf_pos, vcf_rsid = "", "", ""
        for line in bim_handle:
            chrom, snip = line.split()[:2]
            while (vcf_chrom != chrom) or ((vcf_pos != snip) and (vcf_rsid != snip)):
                vcf_line = vcf_handle.readline()
                if vcf_line[0] != "#":
                    fields = vcf_line.split()
                    vcf_chrom, vcf_pos, vcf_rsid = fields[:3]
                    qual = fields[5]
            yield qual, chrom, snip

if __name__ == "__main__":
    qual_feed = yielder(
        bim = argv[1],
        vcf = argv[2]
    )
    quals = []
    for qual, chrom, snip in qual_feed:
        quals.append(qual)
        stdout.write("{qual}\n".format(qual = qual))
        if qual < 150:
            stderr.write("chrom = {chrom}, snip = {snip}, qual = {qual}\n".format(**globals()))
    stderr.write("min(qual) = {minq}, max(qual) = {maxq}\n".format(minq = min(quals), maxq = max(quals)))
