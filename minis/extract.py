#!/usr/bin/env python
from sys import *

def parse_maf(maf_filename):
    with open(maf_filename) as maf_file:
        return set([
            line.split()[1]
            for line in maf_file
        ])

def extract(vcf_filename, positions):
    with open(vcf_filename) as vcf_file:
        for line in vcf_file:
            fields = line.split()
            if len(fields) > 2:
                if (fields[1] in positions) or (fields[2] in positions):
                    stdout.write(line)

if __name__ == "__main__":
    positions = parse_maf(argv[1])
    extract(argv[2], positions)
