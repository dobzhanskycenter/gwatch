#!/usr/bin/env python
from sys import *

def read_dbsnp(dbsnp_filename):
    with open(dbsnp_filename) as dbsnp:
        return dict([
            line.split()[:2]
            for line in dbsnp
        ])

if __name__ == "__main__":
    dbsnp_index = read_dbsnp(argv[1])
    for line in stdin:
        if line[0] == "#":
            stdout.write(line)
        else:
            fields = line.split()
            index = fields[0] + "_" + fields[1]
            try:
                fields[2] = dbsnp_index[index]
            except KeyError:
                fields[2] = "."
            stdout.write("\t".join(fields) + "\n")
