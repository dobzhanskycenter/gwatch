#!/usr/bin/env python
from sys import *
from collections import defaultdict

def get_snips(plink_frq_filename, min_mao, max_maf):
    rare_snips = defaultdict(set)
    too_rare_snips = defaultdict(set)
    with open(plink_frq_filename) as plink_frq:
        for line in plink_frq:
            fields = line.split()
            try:
                chrom = fields[0]
                pos = int(fields[1])
                maf = float(fields[4])
                nchrobs = int(fields[5])
            except ValueError:
                continue
            else:
                mao = round(maf * nchrobs)
                if maf <= max_maf:
                    if mao >= min_mao:
                        rare_snips[chrom].add(pos)
                    else:
                        too_rare_snips[chrom].add(pos)
    return rare_snips, too_rare_snips

def get_quals(vcf_filename):
    with open(vcf_filename) as vcf:
        for line in vcf:
            if line[0] != "#":
                fields = line.split()
                chrom = fields[0]
                pos = int(fields[1])
                qual = float(fields[5])
                yield chrom, pos, qual

if __name__ == "__main__":
    min_mao = int(argv[1])
    max_maf = float(argv[2])
    min_qual = float(argv[3])
    rare_snips, too_rare_snips = get_snips(argv[4], min_mao, max_maf)
    for chrom, pos, qual in get_quals(argv[5]):
        if pos in rare_snips[chrom]:
            if qual < min_qual:
                print("{chrom}\t{pos}\t{qual}".format(**globals()))
    for chrom in too_rare_snips.keys():
        for pos in too_rare_snips[chrom]:
            print("{chrom}\t{pos}\ttoo_rare".format(**globals()))
