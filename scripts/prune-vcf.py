#!/usr/bin/env python
from sys import *
from collections import defaultdict

def get_snips(filenames):
    snips = defaultdict(set)
    rsIDs = set()
    for filename in filenames:
        with open(filename) as snip_list:
            for line in snip_list:
                fields = line.split()
                if len(fields) == 2:
                    chrom = fields[0]
                    pos = int(fields[1])
                    snips[chrom].add(pos)
                elif len(fields) == 1:
                    rsIDs.add(fields[0])
    return snips, rsIDs

if __name__ == "__main__":
    snips_to_remove, rsIDs_to_remove = get_snips(argv[1:])
    for line in stdin:
        if line[0] == "#":
            stdout.write(line)
        else:
            fields = line.split()
            chrom = fields[0]
            pos = int(fields[1])
            rsID = fields[2]
            if (pos not in snips_to_remove[chrom]) and (rsID not in rsIDs_to_remove):
                stdout.write(line)
