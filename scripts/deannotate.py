#!/usr/bin/env python
from sys import *

if __name__ == "__main__":
    for line in stdin:
        if line[0] == "#":
            stdout.write(line)
        else:
            fields = line.split()
            if len(fields) > 2 and fields[2] != ".":
                fields[2] = "."
            print("\t".join(fields))
